# PEMOGRAMAN PLATFORM KHUSUS

## PRAKTIKUM PERTEMUAN 14 : SQLite Practice

## Identitas

NAMA : M ALIF FARHAN

NIM : 222011401

KELAS : 3SI3

## SQLite

Kode Program :
![SS kode program](/SS/01.png "Dokumentasi")

1. Tambah data
   a. tampilan awal ketika memasuki aplikasi
   ![SS tampilan awal ketika memasuki aplikasi](/SS/02.jpg "Dokumentasi")

   b. Masukkan data dan tekan tombol simpan data
   ![SS tampilan Masukkan data](/SS/03.jpg "Dokumentasi")

2. Melihat hasil entri data mahasiswa
   a. tekan button LIHAT DATA, maka akan tampil tampilan data mahasiswa seperti berikut
   ![SS tampilan data mahasiswa](/SS/04.jpg "Dokumentasi")

3. Update data mahasiswa
   a. pada tampilan data mahasiswa, pencet salah satu data mahasiswa maka akan muncul tampilan sebagai berikut :
   ![SS tampilan Update Data](/SS/05.jpg "Dokumentasi")

   b. ganti data lalu pencet button UPDATE MAHASISWA, maka data akan berubah sesuai yang telah diedit dan akan kembali ke tampilan entri data
   ![SS tampilan setelah menekan button UPDATE DATA](/SS/06.jpg "Dokumentasi")

   c. kembali ke tampilan data mahasiswa dan akan terlihat bahwa data mahasiswa telah berubah sesuai dengan yang diedit
   ![SS tampilan data mahasiswa setelah di update](/SS/07.jpg "Dokumentasi")

4. Delete data mahasiswa
   a. kembali ke menu update mahasiswa
   ![SS tampilan Update Data](/SS/08.jpg "Dokumentasi")

   b. pencet button DELETE MAHASISWA maka akan kembali ke tampilan Entri Data
   ![SS tampilan setelah memencet button DELETE MAHASISWA](/SS/09.jpg "Dokumentasi")

   c. cek kembali data mahasiswa, dan akan terlihat bahwa data telah terhapus.
   ![SS tampilan data telah terhapus](/SS/10.jpg "Dokumentasi")
